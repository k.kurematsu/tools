#!/usr/bin/env python
# author k.kurematsu@nskint.co.jp

import sys
import gdb

class AbsViewer():
	def execute(self, cmd):
		return gdb.execute(cmd, to_string = True)

	def _is_hex(self, val):
		try:
			int(val, 16)
			return True
		except ValueError, e:
			return False

	def get_head_addr(self, head):
		ret = head
		if self._is_hex(head) == False:
			try:
				ret = self.get_symbol_addr(head)
			except:
				print 'hexconvert error %s' % (head)
				raise
		return ret

	def get_address_value(self, result):
		tmp = result.split(':')
		tmp2 = tmp[1].strip()
		tmp3 = tmp2.split('<')
		return tmp3[0].strip()

	def get_ptr_value(self, ptr):
		cmd = 'x/ga %s' % (ptr)
		ret = self.execute(cmd)
		next = self.get_address_value(ret)
		return next

	def get_symbol_addr(self, symbol):
		cmd = 'p/x %s' % (symbol)
		ret = self.execute(cmd)
		tmp = ret.split('=')
		return tmp[1].strip()

	def get_offset_value(self, struct_name, member):
		cmd = 'p/x (&(((struct %s*)0)->%s))' % (struct_name, member)
		ret = self.execute(cmd)
		tmp = ret.split('=')
		return tmp[1].strip()

	def dump_struct(self, addr, struct_name):
		cmd = 'p *(struct %s*)%s' % (struct_name, addr)
		ret = self.execute(cmd)
		print(addr)
		print(ret)

	def dump_struct_member(self, addr, struct_name, dump_members):
		print(addr)
		for member in dump_members:
			cmd = 'p ((struct %s*)%s)->%s' % (struct_name, addr, member)
			ret = self.execute(cmd)
			print(ret),
		print

class ListHeadViewer(gdb.Command, AbsViewer):
	"""
	dump struct list_head elements.
	struct list_head {
		struct list_head *next, *prev;
	};
	This command helps to do dump list_head.
	Syntax:
	  Simple list_head dump:
		  (gdb) list_head <head>
	  Entry addr dump:
		  (gdb) list_head <head> <offset of list_head>
		  (gdb) list_head <head> <struct name> <member>
		  (gdb) list_head <head> <struct name> <member> <offset>
	  Entry Struct dump:
		  (gdb) list_head <head> <struct name> <member> <offset> <struct name>
		  e.g. struct connect includes struct list_head named lists
			   struct connect { struct list_head lists; ... };
		  (gdb) list_head <head> connect lists
	  Struct Member dump:
		  (gdb) list_head <head> <struct name> <member> <offset> <struct name> <dump_member>...

	"""

	def __init__(self):
		super(ListHeadViewer, self).__init__('list_head', gdb.COMMAND_SUPPORT, gdb.COMPLETE_NONE, True)

	def invoke(self, arg, from_tty):
		args = arg.split(' ')
		if len(args) < 1:
			return

		self.head = self.get_head_addr(args[0])

		if len(args) == 1:
			self.list_for_each(args)
			return

		if len(args) == 2:
			self.offset = args[1]
			self.list_for_each_entry(args)
			return

		if len(args) == 3:
			self.struct_name = args[1]
			self.member      = args[2]
			self.offset      = '0'
			self.list_for_each_entry_offset(args)
			return

		if len(args) == 4:
			self.struct_name = args[1]
			self.member      = args[2]
			self.offset      = args[3]
			self.list_for_each_entry_offset(args)
			return

		if len(args) == 5:
			self.struct_name = args[1]
			self.member      = args[2]
			self.offset      = args[3]
			self.dump_struct_name = args[4]
			self.list_for_each_entry_offset_dump(args)
			return

		self.struct_name = args[1]
		self.member      = args[2]
		self.offset      = args[3]
		self.dump_struct_name = args[4]
		self.dump_member = args[5:]
		self.list_for_each_entry_member(args)
		return

	def list_for_each(self, args):
		head = self.head;
		next = self.list_next(head)
		nr = 0
		while next != head:
			print(next)
			next = self.list_next(next)
			nr += 1
		print('entry:%s' % (nr))

	def list_for_each_entry(self, args):
		head = self.head;
		next = self.list_next(head)
		nr = 0
		while next != head:
			entry = hex(int(next, 16) - int(self.offset))
			print(entry)
			next = self.list_next(next)
			nr += 1
		print('entry:%s' % (nr))

	def list_for_each_entry_offset(self, args):
		self.offset = self.calc_offset(self.struct_name, self.member, self.offset)
		self.list_for_each_entry(args)

	def list_for_each_entry_offset_dump(self, args):
		self.offset = self.calc_offset(self.struct_name, self.member, self.offset)
		head = self.head;
		next = self.list_next(head)
		nr = 0
		while next != head:
			addr  = hex(int(next, 16) - int(self.offset))
			self.dump_struct(addr, self.dump_struct_name)
			next = self.list_next(next)
			nr += 1
		print('entry:%s' % (nr))

	def list_for_each_entry_member(self, args):
		self.offset = self.calc_offset(self.struct_name, self.member, self.offset)
		head = self.head;
		next = self.list_next(head)
		nr = 0
		while next != head:
			addr  = hex(int(next, 16) - int(self.offset))
			self.dump_struct_member(addr, self.dump_struct_name, self.dump_member)
			next = self.list_next(next)
			nr += 1
		print('entry:%s' % (nr))

	def list_next(self, list):
		return self.get_ptr_value(list)

	def calc_offset(self, struct_name, member, offset):
		_offset = self.get_offset_value(struct_name, member)
		_offset  = int(_offset, 16) + int(offset)
		return _offset

ListHeadViewer()

class TailQViewer(gdb.Command, AbsViewer):
	"""
	dump TAILQ elements.
	This command helps to do dump list_head.
	Syntax:
	  Simple tailq dump:
		  (gdb) tailq <headaddr> <struct name> <member>
		  (gdb) tailq <headaddr> <struct name> <member> <offset>
	  Entry Struct dump:
		  (gdb) tailq <headaddr> <struct name> <member> <offset> <struct name>
	  Struct Member dump:
		  (gdb) tailq <headaddr> <struct name> <member> <offset> <struct name> <offset> <dump member>...
	"""

	def __init__(self):
		super(TailQViewer, self).__init__('tailq', gdb.COMMAND_SUPPORT, gdb.COMPLETE_NONE, True)

	def invoke(self, arg, from_tty):
		args = arg.split(' ')
		if len(args) < 3:
			return

		self.head        = self.get_head_addr(args[0])
		self.struct_name = args[1]
		self.member      = args[2]

		if len(args) == 3:
			self.offset    = '0'
			self.tailq_for_each(args)
			return

		if len(args) == 4:
			self.offset    = args[3]
			self.tailq_for_each(args)
			return

		if len(args) == 5:
			self.offset    = args[3]
			self.dump_name = args[4]
			self.tailq_for_each_dump(args)
			return

		self.offset      = args[3]
		self.dump_name   = args[4]
		self.dump_member = args[5:]
		self.tailq_for_each_dump_member(args)
		return

	def tailq_for_each(self, args):
		head = self.head
		next = self.get_entry_addr(head)
		nr = 0
		while next != '0x0':
			addr  = hex(int(next, 16) - int(self.offset))
			print(addr)
			next = self.tailq_next(next)
			nr += 1
		print('entry:%s' % (nr))

	def tailq_for_each_dump(self, args):
		head = self.head
		next = self.get_entry_addr(head)
		nr = 0
		while next != '0x0':
			addr  = hex(int(next, 16) - int(self.offset))
			self.dump_struct(addr, self.dump_name)
			next = self.tailq_next(next)
			nr += 1
		print('entry:%s' % (nr))

	def tailq_for_each_dump_member(self, args):
		head = self.head
		next = self.get_entry_addr(head)
		nr = 0
		while next != '0x0':
			addr  = hex(int(next, 16) - int(self.offset))
			self.dump_struct_member(addr, self.dump_name, self.dump_member)
			next = self.tailq_next(next)
			nr += 1
		print('entry:%s' % (nr))

	def tailq_next(self, entry):
		cmd = 'x/ga &((struct %s *)%s)->%s.tqe_next' % (self.struct_name, entry, self.member)
		ret = self.execute(cmd)
		next = self.get_address_value(ret)
		return next;

	def get_entry_addr(self, list):
		return self.get_ptr_value(list)

TailQViewer()
